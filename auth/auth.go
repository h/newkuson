package auth

import "git.rwth-aachen.de/h/newkuson/config"

var AdminAuthKey string

func GenerateAdminAuthKey() {
	AdminAuthKey = config.RandStringRunes(10)
}
