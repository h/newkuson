package config

import (
	"bufio"
	"bytes"
	"github.com/BurntSushi/toml"
	"io/ioutil"
	"math/rand"
	"os"
	"time"
)

var ConfigPath string
var Configuration ConfigStruct
var AccessString string
var IsConfigured bool

func RandStringRunes(n int) string {
	rand.Seed(time.Now().UnixNano())
	var letterRunes = []rune("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// SaveSampleConfig sets Configuration to SampleConfig and writes
// SampleConfig as a toml file to the specified config file
func SaveSampleConfig() error {
	Configuration = SampleConfig

	var buf bytes.Buffer
	encoder := toml.NewEncoder(&buf)
	err := encoder.Encode(SampleConfig)
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(ConfigPath, []byte(buf.String()), 0644)
	if err != nil {
		return err
	}

	return nil
}

// LoadConfig tries to load a toml configuration from the given file name
// using the Config class and returns a pointer to that config, or nil
// and an error
func LoadConfig(filename string) (bool, error) {
	data, err := ioutil.ReadFile(filename)
	// If the filename doesn't exist, we simply create a new config.
	// Nothing to worry about here
	if err != nil {
		return false, nil
	}

	if _, err := toml.Decode(string(data), &Configuration); err != nil {
		// If the file exists but is invalid toml, we've got something to
		// worry about. We should panic at this point.
		return false, err
	}

	return Configuration.IsConfigured, nil
}

// LoadConfigAsMap tries to load a toml configuration from the given file name
// and returns it as map[string]interface{}
func LoadConfigAsMap(filename string) (map[string]interface{}, error) {
	data, err := ioutil.ReadFile(filename)
	// If the filename doesn't exist, we simply create a new config.
	// Nothing to worry about here
	if err != nil {
		return nil, err
	}

	var c map[string]interface{}

	if _, err := toml.Decode(string(data), &c); err != nil {
		// If the file exists but is invalid toml, we've got something to
		// worry about. We should panic at this point.
		return nil, err
	}

	return c, nil
}

// SaveConfig tries to save a toml configuration to the given file name
func (c *ConfigStruct) SaveConfig(filename string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	writer := bufio.NewWriter(file)
	enc := toml.NewEncoder(writer)
	return enc.Encode(c)
}
