package config

var SampleConfig = ConfigStruct{
	IsConfigured: false,
	Language: LanguageStruct{
		DefaultLanguage:    "En",
		AvailableLanguages: "De,En",
	},
	BasicData: BasicDataStruct{
		Title:       "New page",
		Description: "",
	},
	ExerciseSystem: ExerciseSystemStruct{
		Enabled:        true,
		MaxNumStudents: -1,
	},
}

type ConfigStruct struct {
	IsConfigured   bool
	Language       LanguageStruct
	BasicData      BasicDataStruct
	ExerciseSystem ExerciseSystemStruct
}

type LanguageStruct struct {
	DefaultLanguage    string
	AvailableLanguages string
}

type BasicDataStruct struct {
	Title       string
	Description string
}

type ExerciseSystemStruct struct {
	Enabled        bool
	MaxNumStudents int
}
