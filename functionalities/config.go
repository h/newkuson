package functionalities

import (
	"bytes"
	"fmt"
	"git.rwth-aachen.de/h/newkuson/auth"
	"git.rwth-aachen.de/h/newkuson/config"
	"git.rwth-aachen.de/h/newkuson/translation"
	"git.rwth-aachen.de/h/newkuson/utils"
	"html/template"
	"net/http"
	"reflect"
	"sort"
	"strconv"
	"strings"
)

type OptionTemplFields struct {
	Name        string
	InputName   string
	Description string
	Value       interface{}
}

func renderFormItem(args interface{}, tpl bytes.Buffer, template *template.Template) string {
	if err := template.ExecuteTemplate(&tpl, "content", args); err != nil {
		panic(err)
	}
	return tpl.String()
}

func sortedKeys(m map[string]interface{}) []string {
	keys := make([]string, len(m))
	i := 0
	for k := range m {
		keys[i] = k
		i++
	}
	sort.Strings(keys)
	return keys
}

// GenerateConfigPage generates a configuration page from data in config.Configuration
func GenerateConfigPage(w http.ResponseWriter, r *http.Request) error {
	var cookie, err = r.Cookie("newkuson-adminauth")
	if err != nil || cookie.Value != auth.AdminAuthKey {
		return utils.RenderOops("NotAuthenticated", "/", w)
	}
	strTemplate, err := template.ParseFiles("templates/config/string.html")
	if err != nil {
		return err
	}

	boolTemplate, err := template.ParseFiles("templates/config/bool.html")
	if err != nil {
		return err
	}

	intTemplate, err := template.ParseFiles("templates/config/int.html")
	if err != nil {
		return err
	}

	floatTemplate, err := template.ParseFiles("templates/config/float.html")
	if err != nil {
		return err
	}

	titleTemplate, err := template.ParseFiles("templates/config/title.html")
	if err != nil {
		return err
	}

	outString := ""
	tpl := bytes.Buffer{}

	// Load the config as map[string]interface{}
	// This is so we can loop through the config
	configuration, err := config.LoadConfigAsMap(config.ConfigPath)

	if err != nil {
		return err
	}

	for _, catKey := range sortedKeys(configuration) {
		category := configuration[catKey]
		mapping, ok := category.(map[string]interface{})
		if !ok {
			continue
		}
		outString += renderFormItem(catKey, tpl, titleTemplate)
		for _, key := range sortedKeys(mapping) {
			value := mapping[key]
			docString, ok := translation.
				ConfigTranslation[config.Configuration.Language.DefaultLanguage][catKey+key]

			if !ok {
				docString = "There doesn't seem to be documentation for this."
			}

			{
				_, ok := value.(string)
				if ok {
					outString += renderFormItem(OptionTemplFields{
						Name:        key,
						InputName:   catKey + "." + key,
						Description: docString,
						Value:       value,
					}, tpl, strTemplate)
					continue
				}
			}
			{
				boolean, ok := value.(bool)
				if ok {
					value := ""
					if boolean {
						value = "checked"
					}
					outString += renderFormItem(OptionTemplFields{
						Name:        key,
						InputName:   catKey + "." + key,
						Description: docString,
						Value:       value,
					}, tpl, boolTemplate)
					continue
				}
			}
			{
				_, ok := value.(int64)
				if ok {
					outString += renderFormItem(OptionTemplFields{
						Name:        key,
						InputName:   catKey + "." + key,
						Description: docString,
						Value:       value,
					}, tpl, intTemplate)
					continue
				}
			}
			{
				_, ok := value.(float64)
				if ok {
					outString += renderFormItem(OptionTemplFields{
						Name:        key,
						InputName:   catKey + "." + key,
						Description: docString,
						Value:       value,
					}, tpl, floatTemplate)
					continue
				}
			}
		}
	}

	r.ParseForm()
	savedNotice := "hidden"
	if r.FormValue("saved") != "" {
		savedNotice = ""
	}

	return utils.Render("config.html", struct {
		Content     template.HTML
		ConfigPath  string
		SavedNotice string
	}{template.HTML(outString), config.ConfigPath, savedNotice},
		translation.ConfigTranslation, w)
}

// WriteConfig saves the config that the user entered in their web interface.
func WriteConfig(w http.ResponseWriter, r *http.Request) error {
	var cookie, err = r.Cookie("newkuson-adminauth")
	if err != nil || cookie.Value != auth.AdminAuthKey {
		return utils.RenderOops("NotAuthenticated", "/", w)
	}

	if r.Method != "POST" {
		return utils.RenderOops("WrongArgs", "/configure", w)
	}

	r.ParseForm()
	form := r.PostForm
	ps := reflect.ValueOf(&config.Configuration)
	s := ps.Elem()
	fields := make(map[string]reflect.Value)
	for key, value := range form {
		cat := strings.Split(key, ".")[0]
		_, ok := fields[key]
		if ok {
			continue
		}
		fmt.Println(key, value)
		catField := s.FieldByName(cat)
		if !catField.IsValid() {
			return utils.RenderOops("WrongArgs", "/configure", w)
		}
		fields[cat] = catField
	}
	for key, value := range form {
		spl := strings.Split(key, ".")
		if len(spl) < 2 {
			return utils.RenderOops("WrongArgs", "/configure", w)
		}
		cat := spl[0]
		opt := spl[1]
		catField := fields[cat]
		optField := catField.FieldByName(opt)
		if !optField.IsValid() || len(value) != 1 {
			return utils.RenderOops("WrongArgs", "/configure", w)
		}
		val := value[0]
		var obj interface{}
		switch optField.Interface().(type) {
		case string:
			obj = val
		case int:
			obj, err = strconv.Atoi(val)
			if err != nil {
				return utils.RenderOops("WrongArgs", "/configure", w)
			}
		case bool:
			fmt.Println(val)
			if val == "on" {
				obj = true
			} else if val == "off" {
				obj = false
			} else {
				return utils.RenderOops("WrongArgs", "/configure", w)
			}
		default:
			panic("Config has an object outside of string, int and bool!?")
		}
		optField.Set(reflect.ValueOf(obj))
	}
	config.Configuration.IsConfigured = true
	config.IsConfigured = true
	config.Configuration.SaveConfig(config.ConfigPath)
	http.Redirect(w, r, "/configure?saved=true", http.StatusFound)
	return nil
}
