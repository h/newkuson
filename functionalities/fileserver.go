package functionalities

import (
	"net/http"
)

// ServeStaticFiles registers the file server on /static/.
func ServeStaticFiles(w http.ResponseWriter, r *http.Request) {
	http.HandleFunc("/static/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, r.URL.Path[1:])
	})
}
