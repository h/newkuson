package functionalities

import (
	"git.rwth-aachen.de/h/newkuson/config"
	"git.rwth-aachen.de/h/newkuson/translation"
	"git.rwth-aachen.de/h/newkuson/utils"
	"net/http"
)

// FrontPage renders the newkuson frontpage..
func FrontPage(w http.ResponseWriter, r *http.Request) error {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return nil
	}
	if !config.IsConfigured {
		http.Redirect(w, r, "/welcome", http.StatusFound)
		return nil
	}
	return utils.Render("frontpage.html", map[string]interface{}{
		"Description": config.Configuration.BasicData.Description,
	}, translation.FrontpageTranslation, w)
}

// About renders the newkuson about page.
func About(w http.ResponseWriter, r *http.Request) error {
	return utils.Render("about.html", nil, translation.AboutTranslation, w)
}
