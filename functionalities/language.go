package functionalities

import (
	"git.rwth-aachen.de/h/newkuson/config"
	"git.rwth-aachen.de/h/newkuson/utils"
	"net/http"
	"strings"
)

// Langugage renders the language chooser, and sets the user's language.
func Language(w http.ResponseWriter, r *http.Request) error {
	languages := strings.Split(config.Configuration.Language.AvailableLanguages, ",")
	return utils.Render("language.html", map[string]interface{}{"Languages": languages}, nil, w)
}
