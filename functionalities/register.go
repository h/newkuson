package functionalities

import (
	"fmt"
	"net/http"
	"os"
)

var functions = map[string]func(http.ResponseWriter, *http.Request){
	"/":            HandleErrorClosure(FrontPage),
	"/welcome":     HandleErrorClosure(Welcome),
	"/welcomecode": HandleErrorClosure(WelcomeCode),
	"/configure":   HandleErrorClosure(GenerateConfigPage),
	"/setconfig":   HandleErrorClosure(WriteConfig),
	"/language":    HandleErrorClosure(Language),
	"/about":       HandleErrorClosure(About),
}

// HandleErrorClosure takes a rendering function that returns an error
// and handles the error correctly, returning a function that can be passed
// to net/http.
func HandleErrorClosure(renderFunc func(
	http.ResponseWriter, *http.Request) error) func(
	http.ResponseWriter, *http.Request) {
	return (func(w http.ResponseWriter, r *http.Request) {
		err := renderFunc(w, r)
		if err != nil {
			fmt.Fprintf(os.Stderr, err.Error())
			w.Write([]byte("While rendering your page, newkuson encountered " +
				"an unexpected error :-(\n" + err.Error() +
				"\nIt would be helpful if you could submit what you did and " +
				"what went wrong to https://git.rwth-aachen.de/h/newkuson " +
				"Thanks :)\n"))
		}
	})
}

func redirect(page string) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		http.Redirect(w, r, page, http.StatusFound)
	}
}

// RegisterFunctionalities maps all handling functions to their respective place.
func RegisterFunctionalities() {
	for loc, fun := range functions {
		http.HandleFunc(loc, fun)
	}
}
