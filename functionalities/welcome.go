package functionalities

import (
	"git.rwth-aachen.de/h/newkuson/auth"
	"git.rwth-aachen.de/h/newkuson/config"
	"git.rwth-aachen.de/h/newkuson/translation"
	"git.rwth-aachen.de/h/newkuson/utils"
	"net/http"
	"time"
)

// Welcome spits out a welcome message for the site admin. If the configuration is not
// configured yet, it will prompt for the config code.
func Welcome(w http.ResponseWriter, r *http.Request) error {
	if !config.IsConfigured {
		return utils.Render("welcome.html", map[string]interface{}{}, translation.WelcomeTranslation, w)
	}
	return nil
}

// WelcomeCode validates the config code, and if it is true, links to GenerateConfigPage.
func WelcomeCode(w http.ResponseWriter, r *http.Request) error {
	r.ParseForm()
	code := r.Form.Get("code")
	if code != config.AccessString {
		return utils.RenderOops("CodeNotCorrect", "/welcome", w)
	}
	auth.GenerateAdminAuthKey()
	cookie := http.Cookie{Name: "newkuson-adminauth",
		Value: auth.AdminAuthKey, Expires: time.Now().Add(time.Hour)}

	http.SetCookie(w, &cookie)

	http.Redirect(w, r, "/configure", http.StatusFound)
	return nil
}
