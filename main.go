package main

import (
	"flag"
	"fmt"
	"git.rwth-aachen.de/h/newkuson/config"
	"git.rwth-aachen.de/h/newkuson/functionalities"
	"git.rwth-aachen.de/h/newkuson/translation"
	"log"
	"net/http"
	"strconv"
)

var port int

const NewkusonVersion = "alpha"

func main() {
	const DefaultPort = 8080

	// Default command line flags
	portPtr := flag.String("port", strconv.Itoa(DefaultPort),
		"The port the server runs on")
	ipStrPtr := flag.String("ip", "localhost", "The ip the server runs on")
	configPath := flag.String("config", "config.toml", "The path of your configuration file")
	flag.Parse()

	functionalities.RegisterFunctionalities()

	http.HandleFunc("/static/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, r.URL.Path[1:])
	})

	translation.LoadTranslations()

	// Load the config
	config.ConfigPath = *configPath
	configured, err := config.LoadConfig(*configPath)
	if err != nil {
		panic(err.Error())
	}
	if !configured {
		config.SaveSampleConfig()
		config.AccessString = config.RandStringRunes(5)
	}

	config.IsConfigured = configured

	ipToVisit := *ipStrPtr

	if config.AccessString != "" {
		fmt.Println("Your configuration access string: " + config.AccessString)
	}

	fmt.Printf("newkuson will start at %s:%s\n", ipToVisit, *portPtr)

	log.Fatal(http.ListenAndServe(*ipStrPtr+":"+string(*portPtr), nil))
}
