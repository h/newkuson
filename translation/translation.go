package translation

import (
	"github.com/BurntSushi/toml"
	"io/ioutil"
)

type TranslationMap map[string]string

type Translation map[string]TranslationMap

var ConfigTranslation Translation
var OopsTranslation Translation
var TemplateTranslation Translation
var WelcomeTranslation Translation
var FrontpageTranslation Translation
var SidebarTranslation Translation
var AboutTranslation Translation
var EmptyTranslation = Translation{}

func LoadTranslations() {
	load(&ConfigTranslation, "translation/config.toml")
	load(&OopsTranslation, "translation/oops.toml")
	load(&TemplateTranslation, "translation/template.toml")
	load(&WelcomeTranslation, "translation/welcome.toml")
	load(&FrontpageTranslation, "translation/frontpage.toml")
	load(&SidebarTranslation, "translation/sidebar.toml")
	load(&AboutTranslation, "translation/about.toml")
}

func load(translation *Translation, filename string) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		panic(err)
	}

	if _, err := toml.Decode(string(data), translation); err != nil {
		panic(err)
	}
}
