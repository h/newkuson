package utils

import (
	"git.rwth-aachen.de/h/newkuson/config"
	"git.rwth-aachen.de/h/newkuson/translation"
)

type SidebarLink struct {
	Title string
	Href  string
}

func getSidebar() []SidebarLink {
	transl := translation.SidebarTranslation[config.Configuration.Language.DefaultLanguage]
	return []SidebarLink{
		SidebarLink{transl["Frontpage"], "/"},
		SidebarLink{transl["Language"], "/language"},
		SidebarLink{transl["About"], "/about"},
	}
}
