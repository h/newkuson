package utils

import (
	"git.rwth-aachen.de/h/newkuson/config"
	"git.rwth-aachen.de/h/newkuson/translation"
	"html/template"
	"net/http"
)

// Render takes a template location, the title of the page, the arguments for rendering
// and the response writer and subsequently renders your page, template included
func Render(templateLoc string, args interface{},
	transl translation.Translation, writer http.ResponseWriter) error {
	template, err := template.ParseFiles("templates/template.html", "templates/"+templateLoc)
	if err != nil {
		return err
	}

	data := map[string]interface{}{
		"Version":      "alpha",
		"SidebarLinks": getSidebar(),
		"T":            transl[config.Configuration.Language.DefaultLanguage],
		"TT":           translation.TemplateTranslation[config.Configuration.Language.DefaultLanguage],
		"Title":        config.Configuration.BasicData.Title,
		"Data":         args,
	}

	err = template.ExecuteTemplate(writer, "template", data)
	if err != nil {
		return err
	}
	return nil
}

// RenderOops takes an index to OopsTranslation, and presents it to the user
func RenderOops(message string, goBackLink string, writer http.ResponseWriter) error {
	return Render("oops.html", struct {
		Message    string
		GoBackLink string
	}{translation.OopsTranslation[config.Configuration.Language.DefaultLanguage][message],
		goBackLink}, translation.OopsTranslation, writer)
}
